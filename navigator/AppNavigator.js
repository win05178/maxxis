
import LoginScreen from "../screen/login/login";
import {createStackNavigator,createAppContainer} from "react-navigation";

const AppNavigator  = createStackNavigator  ({
        Login: {screen: LoginScreen},
});
export default createAppContainer(AppNavigator) ;