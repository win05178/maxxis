import axios from 'axios'

const Server ="https://maxxis-warranty.com/api/api/";



const httpServices = {
    get:function (url,callback) {
        axios.get(Server+url)
            .then(response => {if(callback!=null)callback(response.data)})
    },
    post:function (url,data,callback) {
        axios.post(Server+url,data)
            .then(response => {if(callback!=null)callback(response.data)})
    }
};

export default httpServices;