import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity, Dimensions,FlatList,TouchableHighlight
} from 'react-native';
import {Button, Icon, Avatar, Card,Divider,Header} from 'react-native-elements';
import { NavigationBar } from 'navigationbar-react-native';
import httpServices from "../../services/http";
import Images from "../../config1";
import FlexImage from "react-native-flex-image";
import HTML from "react-native-render-html";
import Tags from "react-native-tags/Tags";

const styles = StyleSheet.create({
    lineStyle:{
        borderWidth: 0.5,
        borderColor:'black',
        margin:10,
    }
});

export default class ProductViewScreen extends React.Component {

    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>{navigation.getParam('product', 'DefaultTitle')}</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })

    state = {
        navigation:this.props.navigation,
        productDetail:[],
        productTag:[],
        productModel:[]
    };
    ComponentLeft= () => {
        return(
            <View style={{ flex: 1 }}>
                <Text  style={styles.fontAll}>สินค้า</Text>
            </View>
        );
    };

    constructor(props){
        super(props);
        var me =this;
        httpServices.get("product/getProductById.php?id="+ this.state.navigation.getParam('id', 'DefaultTitle'),function (data) {
            me.setState({productDetail:data.data[0]});
        });
        httpServices.get("product/getProductTag.php?id="+ this.state.navigation.getParam('id', 'DefaultTitle'),function (data) {

            var tag = [];
            data.data.forEach(function (data) {
                tag.push(data.tag);
            })
            me.setState({productTag:tag});
        });
        httpServices.get("product/getProductModel.php?id="+ this.state.navigation.getParam('id', 'DefaultTitle'),function (data) {
            me.setState({productModel:data.data});
        });
    }


    render() {
        var width = Dimensions.get('window').width;
        return (
            <View style={{backgroundColor:'#f9f9f9',flex:1}}>
                <ScrollView>
                    <View >
                        <View style={{ justifyContent: 'center',
                            alignItems: 'center'}}>
                        {this.state.productDetail.image != null ?
                            <FlexImage  source={{uri: this.state.productDetail.image}} style={{ width:200,height:180}} />:<View/>

                        }
                        </View>
                        <View style = {styles.lineStyle} />
                        <Tags
                            initialTags={this.state.productTag}
                            readonly={true}
                            tagContainerStyle={{ backgroundColor:'#f04c24' }}
                            tagTextStyle={{ color:'white' ,fontFamily: 'kanit'}}
                        />
                        <View style={{paddingLeft: 20,paddingRight: 20}}>
                            <Text style={{fontFamily: 'kanit'}} >{this.state.productDetail.product_name} </Text>
                        <HTML html={this.state.productDetail.product_detail} imagesMaxWidth={Dimensions.get('window').width} baseFontStyle={{fontFamily: 'kanit'}}   />
                        </View>
                        <View style = {{backgroundColor:'#333333',color:'white',padding:10}} >
                            <Text style={{color:"white",fontFamily: 'kanit'}}>Specification</Text>
                        </View>
                        {
                            this.state.productModel.map((item, index) => (
                                <View style={{padding:20}}>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={{width:width/2,color:'#f04c24'}}>Item</Text>
                                        <Text style={{width:width/2,color:'#f04c24'}}>Size</Text>
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={{width:width/2}}>{item.product_item}</Text>
                                        <Text style={{width:width/2}}>{item.product_size}</Text>
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={{width:width/2,color:'#f04c24'}}>Description</Text>
                                        <Text style={{width:width/2,color:'#f04c24'}}>Rating</Text>
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={{width:width/2}}>{item.product_desc}</Text>
                                        <Text style={{width:width/2}}>{item.product_play_rating}</Text>
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={{width:width/2,color:'#f04c24'}}>Side Wall</Text>

                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={{width:width/2}}>{item.product_sidewall}</Text>
                                    </View>
                                </View>


                            ))
                        }

                    </View>


                </ScrollView>
            </View>
        );
    }
}
