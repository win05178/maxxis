import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity, Dimensions,
} from 'react-native';
import { Button,Icon ,Avatar} from 'react-native-elements';
import { NavigationBar } from 'navigationbar-react-native';
import httpServices from "../../services/http";
import Images from "../../config1";
import FlexImage from "react-native-flex-image";

export default class ProductScreen extends React.Component {

    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>สินค้า</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })

    state = {
        navigation:this.props.navigation,
        product:[]
    };
    ComponentLeft= () => {
        return(
            <View style={{ flex: 1 }}>
                <Text  style={styles.fontAll}>สินค้า</Text>
            </View>
        );
    };

    constructor(props){
        super(props);
        var me =this;
        httpServices.get("product/getProductCate.php",function (data) {
            me.setState({product:data.data});
        });
    }


    render() {

        return (
            <View >


                <ScrollView>
                    {
                        this.state.product.map((item, index) => (

                                <TouchableOpacity
                                    key = {item.id} onPress={()=>this.state.navigation.push("ProductDetail", {product: item.name})}>
                                    <View  >
                                        <FlexImage   source={{uri: item.image}}  />
                                    </View>
                                </TouchableOpacity>
                            )
                        )
                    }
                </ScrollView>
            </View>
        );
    }
}
