import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity, Dimensions,FlatList,TouchableHighlight
} from 'react-native';
import {Button, Icon, Avatar, Card} from 'react-native-elements';
import { NavigationBar } from 'navigationbar-react-native';
import httpServices from "../../services/http";
import Images from "../../config1";
import FlexImage from "react-native-flex-image";
import HTML from "react-native-render-html";

export default class ProductDetailScreen extends React.Component {

    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>{navigation.getParam('product', 'DefaultTitle')}</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })

    state = {
        navigation:this.props.navigation,
        productDetail:[]
    };
    ComponentLeft= () => {
        return(
            <View style={{ flex: 1 }}>
                <Text  style={styles.fontAll}>สินค้า</Text>
            </View>
        );
    };

    constructor(props){
        super(props);
        var me =this;
        httpServices.get("product/getProduct.php?product_cate="+ this.state.navigation.getParam('product', 'DefaultTitle'),function (data) {
            me.setState({productDetail:data.data});
            console.log(data.data)
        });
    }

    submit = {
        marginRight:40,
        marginLeft:40,
        marginTop:10,
        paddingTop:20,
        paddingBottom:20,
        backgroundColor:'#68a0cf',
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#fff'
    }

    render() {
        var width = Dimensions.get('window').width;
        return (
            <View style={{backgroundColor:'#f9f9f9',flex:1}}>
                <ScrollView>

                            <FlatList
                                data={this.state.productDetail}
                                numColumns={2}
                                renderItem={({item}) =>
                                    <TouchableOpacity key={item.id} onPress={()=>this.state.navigation.push("ProductView", {product: item.product_name,id: item.id})} >
                                        <Card containerStyle={{borderWidth: 0,borderColor: 'white',width: Dimensions.get('window').width/2-30}}>
                                            <FlexImage   source={{uri: item.image_thumbnail}}  />
                                            <Button containerViewStyle={{borderRadius:25, borderColor:'black'}} buttonStyle={{borderRadius:25, backgroundColor: 'white', borderColor:'black'}} textStyle={{color:'black',fontFamily: 'kanit'}} title={item.product_name}/>
                                        </Card>
                                    </TouchableOpacity>
                                }
                            />
                </ScrollView>
            </View>
        );
    }
}
