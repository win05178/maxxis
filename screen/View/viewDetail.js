import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity ,
    Dimensions
} from 'react-native';
import { Button,Icon ,Avatar} from 'react-native-elements';
import { NavigationBar } from 'navigationbar-react-native';
import httpServices from "../../services/http";
import Images from "../../config1";
import HTML from 'react-native-render-html';
import FlexImage from "react-native-flex-image";


export default class ViewDetailScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        // title: navigation.getParam('head', 'DefaultTitle'),
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>{navigation.getParam('head', 'DefaultTitle')}</Text>,
        headerStyle: {
            backgroundColor: '#333333',

        },
        titleStyle:{
            fontFamily: 'kanit'
        }
    })

    state = {
        navigation:this.props.navigation,
        width:0,
        height:0
    };

    constructor(props){
        super(props);

    }

    render() {
        var width = Dimensions.get('window').width;

        //Image.getSize(uri: this.state.navigation.getParam('image', ''), (width, height) => {this.setState({width, height})})
        return (
            <View >

                <ScrollView >

                    {this.state.navigation.getParam('image', '') != '' ?
                         <FlexImage    source={{uri: this.state.navigation.getParam('image', '')}} />:<View/>

                    }
                    <HTML html={this.state.navigation.getParam('data', '')} imagesMaxWidth={Dimensions.get('window').width} baseFontStyle={{fontFamily: 'kanit'}} containerStyle={{paddingLeft: 20,paddingRight: 20}}/>

                </ScrollView>
            </View>
        )
    }
}