import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity, AsyncStorage,
} from 'react-native';
import { Button,Icon ,Avatar} from 'react-native-elements';
import { NavigationBar } from 'navigationbar-react-native';
import httpServices from "../../services/http";
import Images from "../../config1";

const styles = StyleSheet.create({
    lineStyle:{
        borderWidth: 0.5,
        borderColor:'black',
        margin:10,
    }
});

export default class MenuScreen extends React.Component {

    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>เมนู</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })
    state = {
        navigation:this.props.navigation,
        promotion:[]
    };


    constructor(props){
        super(props);
        var me =this;

    }

    async _logout(token){
        try {
            await AsyncStorage.removeItem('token');
            this.state.navigation.popToTop()
        } catch (error) {
            // Error saving data
        }
    }
    render() {
        return (
            <View style={{ flex: 1}}>

                <ScrollView>
                   <TouchableOpacity style={{marginBottom: 5}} >
                        <Text style={{ fontFamily: 'kanit',padding:10}}>แก้ไขข้อมูลสมาชิก</Text>
                       <View style = {styles.lineStyle} />
                   </TouchableOpacity>
                    <TouchableOpacity  style={{marginBottom: 5}} >
                        <Text style={{ fontFamily: 'kanit',padding:10}}>เปลี่ยนรหัสผ่าน</Text>
                        <View style = {styles.lineStyle} />
                    </TouchableOpacity>
                    <TouchableOpacity  style={{marginBottom: 5}}  onPress={()=> {

                            this.state.navigation.push("HistoryView")

                    }} >
                        <Text style={{ fontFamily: 'kanit',padding:10}}>ประวัติการสั่งซื้อ/เคลมสินค้า</Text>
                        <View style = {styles.lineStyle} />
                    </TouchableOpacity>
                    <TouchableOpacity  style={{marginBottom: 5}} >
                        <Text style={{ fontFamily: 'kanit',padding:10}}>ข้อมูลบริการ</Text>
                        <View style = {styles.lineStyle} />
                    </TouchableOpacity>
                    <TouchableOpacity  style={{marginBottom: 5}}  onPress={()=> {
                        this._logout()}} >
                        <Text style={{ fontFamily: 'kanit',padding:10}}>ออกจากระบบ</Text>
                        <View style = {styles.lineStyle} />
                    </TouchableOpacity>

                </ScrollView>
            </View>
        );
    }
}
