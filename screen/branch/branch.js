import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity, Dimensions, FlatList, TouchableHighlight, AsyncStorage, Picker,Platform
} from 'react-native';
import {Button, Icon, Avatar, Card, Divider, Header, CheckBox} from 'react-native-elements';
import { NavigationBar } from 'navigationbar-react-native';
import httpServices from "../../services/http";
import Images from "../../config1";
import FlexImage from "react-native-flex-image";
import HTML from "react-native-render-html";
import Tags from "react-native-tags/Tags";
import { MapView,Constants, Location, Permissions } from 'expo';

const styles = StyleSheet.create({
    lineStyle:{
        borderWidth: 0.5,
        borderColor:'black',
        margin:10,
    }
});

export default class BranchViewScreen extends React.Component {


    componentWillMount() {
        if (Platform.OS === 'android' && !Constants.isDevice) {
            this.setState({
                errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
            });
        } else {
           console.log(this._getLocationAsync()) ;
        }
    }

    _getLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Permission to access location was denied',
            });
        }

        let location = await Location.getCurrentPositionAsync({ enableHighAccuracy: true });
        return location;
        // if(this.location!=null) {
        //     console.log(this.location)
        //     this.setState({
        //         region: {
        //             latitude: location.latitude,
        //             longitude: location.longitude
        //         }
        //     });
        //     this.setState({location});
        // }
    };

    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>สาขา</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })

    state = {
        navigation:this.props.navigation,
        cliam:this.props.navigation.getParam("cliam"),
        branch_data:[],
        location: {
            latitude:null,
            longitude:null
        },
        errorMessage: null,
        region: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }
    };



    constructor(props){
        super(props);

        console.log(this.state.cliam);
        var me =this;
        httpServices.get("province/getProvince.php",function (data) {
            me.setState({province:data.data});
            me.getBranch(data.data[0].name);
        });
    }





    render() {
        var width = Dimensions.get('window').width;
        return (
            <View style={{flex:1}}>
                <MapView
                    style={{ flex: 1 }}
                    region={this.state.region}

                />
            </View>
        );
    }
}
