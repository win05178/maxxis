import React from "react";
import {StyleSheet, Text, View, Image, TextInput, TouchableOpacity} from 'react-native';
import { Button } from 'react-native-elements';
import FlexImage from "react-native-flex-image";
import httpServices from "../../services/http";
import { AsyncStorage } from "react-native"



export default class LoginScreen extends React.Component {
    state = { Email: '' ,Password:'',   navigation:this.props.navigation,token:null};
    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>Login</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })


    async  logIn() {
        var me = this;
        const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync(
            "575592706213638",
            {
                permissions: ["public_profile","email"],
                behavior:'browser'
            }
        );
        if (type === "success") {
            const response = await fetch(`https://graph.facebook.com/me?fields=id,name,email&access_token=${token}`);
            var res = JSON.parse(response._bodyText);

            httpServices.post("user/login.php",{
                email:res.email
            },function (data) {
                if(data.success==true) {
                    me._saveToken(data.data.token)
                    me.state.navigation.popToTop()
                }else{
                    alert("ไม่สามารถเข้าสู่ระบบได้");
                }
            });
            // Handle successful authentication here
        } else {
            // Handle errors here.
        }
    }
    async _saveToken(token){
        try {
            await AsyncStorage.setItem('token', token);
        } catch (error) {
            // Error saving data
        }
    }

    localLogIn(){
        var me = this;
        httpServices.post("user/login.php",{
            email:this.state.Email,
            password:this.state.Password
        },function (data) {
            if(data.success==true) {
                me._saveToken(data.data.token)
                me.state.navigation.popToTop()
            }else{
                alert("ไม่สามารถเข้าสู่ระบบได้");
            }
        });
    }
    render() {
        return (
            <View style={{flex:1,backgroundColor:"#333333"}}>
                <View  style={{ justifyContent: 'center',
                    alignItems: 'center'}}>
                        <Image resizeMode={'cover'} style={{width:300, height: 200 }}
                            source={require('../../assets/image/logo-web.png')}
                        />
                </View>
                <View style={{flexDirection: 'row'}}>

                    <Image  style={{width:50, height: 50 }}
                           source={require('../../assets/image/icon_email-web.png')}
                    />

                    <TextInput
                        textContentType={'emailAddress'}
                        keyboardType={'email-address'}
                        style={styles.fontInputLogin}
                        placeholder='E-Mail'
                        onChangeText={(text) => {
                            this.state.Email = text;
                            this.setState(this.state);
                        }}
                        value={this.state.Email}
                    />
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Image resizeMode={'cover'} style={{width:50, height: 50 }}
                           source={require('../../assets/image/icon_password-web.png')}
                    />
                    <TextInput
                        textContentType={'password'}
                        keyboardType={'default'}
                        placeholder='รหัสผ่าน'
                        style={styles.fontInputLogin}
                        secureTextEntry={true}
                        onChangeText={(text) => {
                            this.state.Password = text;
                            this.setState(this.state);
                        }}
                        value={this.state.Password}
                    />
                </View>
                <View   >
                    <Button textStyle={styles.buttonLogin} title={"เข้าสู่ระบบ"} onPress={()=>this.localLogIn()}  backgroundColor='red' buttonStyle={{marginTop:10}} />
                </View>
                <View   >
                    <Button textStyle={styles.buttonLogin} title={"เข้าสู่ระบบ ด้วย Facebook"} onPress={()=>this.logIn()}  backgroundColor='#3b5998' buttonStyle={{marginTop:10}} />

                </View>

                <View   >
                    <Button textStyle={{color:'#5c6573', fontFamily:'kanit'}} title={"ลืมรหัสผ่าน ?"}  transparent={true} />
                </View>
                <View
                    style={{
                        borderBottomColor: 'black',
                        borderBottomWidth: 1,
                    }}
                />
                <View style={{ alignItems: 'center'}}>
                <View style={{flexDirection: 'row'}}>
                    <Button textStyle={styles.buttonLogin} title={"ลงทะเบียนสมัครใช้งาน"}  onPress={()=>this.state.navigation.push("Register1")}   transparent={true} />

                </View>
                </View>
            </View>
        )
    }
}
