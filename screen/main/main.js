import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  COLOR,
  Image,
  TouchableOpacity,
  Animated,
  Button,
} from 'react-native';
import Expo from 'expo';
import { AppLoading, Font } from 'expo';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { Icon } from 'react-native-elements';
import HomeScreen from '../home/home';
import PromotionScreen from '../promotion/promotion';
import NotificationScreen from '../notification/notification';
import Images from '../../config1';


export default class MainScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
     FirstRoute = () => <HomeScreen navigation={this.props.navigation}/>;
     SecondRoute = () => <PromotionScreen navigation={this.props.navigation}/>;
     ThreeRoute = () => <NotificationScreen navigation={this.props.navigation}/>;
  state = {
    isLoadingComplete: false,
    index: 0,
    routes: [
      { key: 'first', title: 'First', icon: Images.tabMenu.home },
      { key: 'second', title: 'Second', icon: Images.tabMenu.promotion },
        { key: 'three', title: 'Three', icon: Images.tabMenu.three },
    ],
  };

  _renderLabel = route => {
    return (
      <View>
        <Image source={route.route.icon} style={{ width: 50, height: 50 }} />
      </View>
    );
  };

  _renderTabBar = props => {
    return (
      <TabBar
        {...props}
        style={{ backgroundColor: '#333333', height: 50 }}
        renderLabel={this._renderLabel}
        indicatorStyle={{ backgroundColor: '#333333' }}
      />
    );
  };
  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <TabView
          navigationState={this.state}
          renderScene={SceneMap({
            first: this.FirstRoute,
            second: this.SecondRoute ,
              three:this.ThreeRoute,
          })}
          tabBarPosition="bottom"
          renderTabBar={this._renderTabBar}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{ width: Dimensions.get('window').width }}
        />
      );
    }
    
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Font.loadAsync({
        // This is the font that we are using for our tab bar

        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        kanit: require('../../assets/fonts/kanit_regular.ttf'),
      }),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}
