import React from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Picker,
    AsyncStorage,
    Dimensions
} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Button } from 'react-native-elements';
import httpServices from "../../services/http";

export default class getProductScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>ลงทะเบียนยาง</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })
    state = { product_cate:null,productCate:[],serialNumber:null,model_id:null,model:[],spec_id:null,spec:[],token:null ,province_id:null,branch_data:[],navigation:this.props.navigation,  isDateTimePickerVisible: false,};

    async _getToken () {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                this.reload(value)
            }

        } catch (error) {
            // Error retrieving data
        }
    }
    constructor(prop){
        super(prop)
        this._getToken();



    }
    reload(token){
        var me =this;
        httpServices.get("product/getProductCate.php",function (data) {

            me.setState({productCate:data.data});
            me.model(0);
        });

    }
    model(id){

        var me =this;
        me.setState({product_cate:this.state.productCate[id].id});
        httpServices.get("product/getProduct.php?product_cate="+this.state.productCate[id].name,function (data) {

            me.setState({model:data.data});
            me.spec(0);
        });
    }
    spec(id){

        var me =this;
        me.setState({model_id:this.state.model[id].id});
        httpServices.get("product/getProductModel.php?id="+this.state.model[id].id,function (data) {

            me.setState({spec:data.data});
            me.setState({spec_id:data.data[0].id});
        });
    }

    addProduct(){
        var sn ={
            sn:this.state.serialNumber,
            delete_flag:0,
            status:0,
            category:this.state.product_cate,
            model:this.state.model_id,
            size:this.state.spec_id
        }
        var callback = this.state.navigation.getParam('add', null)
        callback(sn);
        this.state.navigation.pop();
    }
    render() {
        var width = Dimensions.get('window').width;
        return (
            <View style={{padding:20}}>
                <View>
                    <Text style={{color:'#f04c24'}}>Category</Text>
                    <Picker
                        selectedValue={this.state.product_cate}
                        style={{ height: 50, width: width }}
                         onValueChange={(itemValue, itemIndex) => this.model(itemIndex) }
                    >
                        {
                            this.state.productCate.map((item, index) => (
                                <Picker.Item label={item.name} value={item.id}  />
                            ))
                        }
                    </Picker>
                </View>

                <View>
                    <Text style={{color:'#f04c24'}}>รุ่น</Text>
                    <Picker
                        selectedValue={this.state.model_id}
                        style={{ height: 50, width: width }}
                         onValueChange={(itemValue, itemIndex) => this.spec(itemIndex) }
                    >
                        {
                            this.state.model.map((item, index) => (
                                <Picker.Item label={item.product_name} value={item.id}  />
                            ))
                        }
                    </Picker>
                </View>


                <View>
                    <Text style={{color:'#f04c24'}}>ขนาด</Text>
                    <Picker
                        selectedValue={this.state.spec_id}
                        style={{ height: 50, width: width }}
                         onValueChange={(itemValue, itemIndex) => this.setState({spec_id:itemValue}) }
                    >
                        {
                            this.state.spec.map((item, index) => (
                                <Picker.Item label={item.product_size} value={item.id}  />
                            ))
                        }
                    </Picker>
                </View>
                <View>
                    <Text style={{color:'#f04c24'}}>Serial Number</Text>
                    <TextInput

                        style={{  fontFamily:'kanit',}}
                        placeholder='กรุณากรอกข้อมูล'
                        onChangeText={(text) => {
                            this.state.serialNumber = text;
                            this.setState(this.state);
                        }}
                        value={ this.state.serialNumber}
                    />
                </View>

                <View>
                    <Button textStyle={{color:'#5c6573', fontFamily:'kanit'}} title={"เพิ่มรายการ"} onPress={()=>this.addProduct()}  transparent={true} />
                </View>


            </View>
        )
    }
}