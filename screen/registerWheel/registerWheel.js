import React from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Picker,
    AsyncStorage,
    Dimensions
} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Button } from 'react-native-elements';
import httpServices from "../../services/http";

export default class RegisterWheelScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>ลงทะเบียนยาง</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })
    state = { register:{
        car_id:null,branch:null,car_num:null,sn:[]
        },car:[],province:[],token:null ,province_id:null,branch_data:[],navigation:this.props.navigation,  isDateTimePickerVisible: false,};

    async _getToken () {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                this.reload(value)
            }

        } catch (error) {
            // Error retrieving data
        }
    }
     constructor(prop){
        super(prop)
         this._getToken();



    }
    reload(token){
        this.setState({token:token})
        var me =this;
        httpServices.get("car/getCar.php?token="+token,function (data) {
            var d = data.data;
            d.push({car_id:"อื่น ๆ",car_province:"",id:-999});
            me.setState({car:d});
            var register = me.state.register;
            register.car_id = d[0].car_id;
            me.setState({register:register})
    });

        httpServices.get("province/getProvince.php",function (data) {
            me.setState({province:data.data});
            me.getBranch(data.data[0].name);
        });
    }
    getBranch(itemValue){
        this.setState({province_id:itemValue})
        var me = this;
        httpServices.get("branch/getBranch.php?province="+itemValue,function (data) {
            me.setState({branch_data:data.data});
            var register = me.state.register;
            register.branch = data.data[0].id;
            me.setState({register:register})
        });
    }
    addSn(item){
        //console.log(item)
        var register = this.state.register;
        register.sn.push(item)
        this.setState({register:register});
    }
    delete(item){
        //console.log(item)
        var sn = this.state.register.sn;
        sn.splice(item,1);
        this.setState({register:{sn:sn}});
    }
    render() {
        var width = Dimensions.get('window').width;
        return (
            <View style={{padding:20}}>
                <View>
                    <Text style={{color:'#f04c24'}}>เลือกรถ</Text>
                    <Picker
                        selectedValue={this.state.register.car_id}
                        style={{ height: 50, width: width }}
                        onValueChange={(itemValue, itemIndex) => {
                            var register = this.state.register;
                            register.car_id = itemValue;
                            this.setState({register:register})} }>
                        {
                            this.state.car.map((item, index) => (
                                <Picker.Item label={item.car_id} value={item.id}  />
                            ))
                        }
                    </Picker>
                </View>
                {(this.state.register.car_id==-999)?(
                    <View>
                        <Text style={{color:'#f04c24'}}>ระบุรถรุ่นอื่น ๆ</Text>
                        <TextInput

                            style={{  fontFamily:'kanit',}}
                            placeholder='กรุณากรอกข้อมูล'
                            onChangeText={(text) => {

                                this.state.register.car_num = text;
                                this.setState(this.state);
                            }}
                            value={ this.state.register.car_num}
                        />
                    </View>
                ):null}

                <View>
                    <Text style={{color:'#f04c24'}}>จังหวัดร้านที่ซื้อ</Text>
                    <Picker
                        selectedValue={this.state.province_id}
                        style={{ height: 50, width: width }}
                        onValueChange={(itemValue, itemIndex) =>this.getBranch(itemValue)   }>
                        {
                            this.state.province.map((item, index) => (
                                <Picker.Item label={item.name} value={item.name}  />
                            ))
                        }
                    </Picker>
                </View>
                <View>
                    <Text style={{color:'#f04c24'}}>ชื่อร้านที่ซื้อ</Text>
                    <Picker
                        selectedValue={this.state.register.branch}
                        style={{ height: 50, width: width }}
                        onValueChange={(itemValue, itemIndex) =>{
                            var register = this.state.register;
                            register.branch = itemValue;
                            this.setState({register:register})   }}>
                        {
                            this.state.branch_data.map((item, index) => (
                                <Picker.Item label={item.branch_name} value={item.id}  />
                            ))
                        }
                    </Picker>
                </View>
                <View>
                    <Text style={{color:'#f04c24'}}>รายการสินค้า</Text>
                </View>
                {
                    this.state.register.sn.map((item, index) => (
                        <View style={{flexDirection:'row'}}>
                            <Text style={{paddingTop: 10}}>{item.sn}</Text>
                            <Button textStyle={{color:'red', fontFamily:'kanit'}} title={"delete"}    onPress={()=>this.delete(index) }  transparent={true} />
                        </View>

                    ))
                }
                <View>
                    <Button textStyle={{color:'#5c6573', fontFamily:'kanit'}} title={"เพิ่มรายการ"} onPress={()=>this.state.navigation.push("AddProduct",{"add":(item) => this.addSn(item) })}  transparent={true} />
                </View>

                <View>
                    <Button textStyle={{color:'#5c6573', fontFamily:'kanit'}} title={"ถ่ายรูปใบเสร็จ"} onPress={()=>this.state.navigation.push("takePhoto",{"register": this.state.register,"token":this.state.token})}  transparent={true} />
                </View>

            </View>
        )
    }
}