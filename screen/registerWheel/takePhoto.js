import React from 'react';
import {Text, View, TouchableOpacity, AsyncStorage,Alert} from 'react-native';
import { Camera, Permissions } from 'expo';
import {Button} from "react-native-elements";
import httpServices from "../../services/http";


export default class takePhoto extends React.Component {
    state = {
        hasCameraPermission: null,
        type: Camera.Constants.Type.back,navigation:this.props.navigation,token:null,save:false
    };

    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    }
    async _getToken () {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                this.reload(value)
            }

        } catch (error) {
            // Error retrieving data
        }
    }

    snap = async () => {
        if(!this.state.save) {
            this.setState({save:true});
            if (this.camera) {
                var me = this;
                let photo = await this.camera.takePictureAsync({quality:1, base64: true});
                Alert.alert(
                    'แจ้งเตือน',
                    'ยืนยันการบันทึก',
                    [
                        {text: 'Cancel', onPress: () =>   this.setState({save:false}), style: 'cancel'},
                        {
                            text: 'OK', onPress: () => {
                                me._getToken();
                                var register = me.state.navigation.getParam('register', null);
                                register.delete_flag = 0;
                                register.status = 0;
                                register.image_receipt = photo.base64;
                                   console.log({data:JSON.stringify(register),token: me.state.navigation.getParam('token', null)})
                                httpServices.post("register_wheel/register.php", {
                                    data: JSON.stringify(register),
                                    token: me.state.navigation.getParam('token', null)
                                }, function (data) {
                                    me.setState({save:false})
                                    alert("บันทึกสำเร็จ")
                                    me.state.navigation.popToTop()
                                });

                            }
                        },
                    ],
                    {cancelable: false}
                )

            }
        }else {
            alert("กำลังบันทึกข้อมูล")
        }
        };

        getBase64(file, cb) {
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                cb(reader.result)
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
        };
    }
    render() {

        const { hasCameraPermission } = this.state;
        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => { this.camera = ref; }}>
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                flexDirection: 'row',
                            }}>

                        </View>
                    </Camera>
                    <View>
                        <Button textStyle={{color:'#5c6573', fontFamily:'kanit'}} title={"ถ่ายรูป"} onPress={()=>this.snap()} />
                    </View>
                </View>
            );
        }
    }
}