import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity ,
} from 'react-native';
import { Button,Icon ,Avatar} from 'react-native-elements';
import { NavigationBar } from 'navigationbar-react-native';
import httpServices from "../../services/http";
import Images from "../../config1";

export default class NotificationScreen extends React.Component {

    state = {
        navigation:this.props.navigation,
        notification:[]
    };
    ComponentLeft= () => {
        return(
            <View style={{ flex: 1 }}>
                <Text  style={styles.fontAll}>แจ้งเตือน</Text>
            </View>
        );
    };

    constructor(props){
        super(props);
        var me =this;
        httpServices.get("notifications/getNotification.php",function (data) {
            me.setState({notification:data.data});
            console.log(data.data)
        });
    }


    render() {
        return (
            <View style={{ flex: 1,marginBottom: 25 }}>
                <NavigationBar
                    componentLeft   = { this.ComponentLeft}

                    navigationBarStyle= {{ backgroundColor: '#333333' }}
                    statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#333333' }}
                />

                <ScrollView>
                    {
                        this.state.notification.map((item, index) => (

                                <TouchableOpacity
                                    key = {item.id} style={{marginBottom: 5}}  onPress={()=>this.state.navigation.push("ViewDetail", {head: item.name,data: item.detail,image:item.image_url})}>
                                    <View  style={{flexDirection:'row'}}>

                                        <Image   source={{uri: item.image_url}} style={{width:100,height:100}} />

                                        <View  style={{paddingLeft: 20,paddingRight: 20}}>
                                            <Text style={{ fontFamily: 'kanit'}}>{item.name}</Text>
                                        </View>

                                    </View>
                                </TouchableOpacity>
                            )
                        )
                    }
                </ScrollView>
            </View>
        );
    }
}
