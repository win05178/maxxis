import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity, AsyncStorage, Dimensions,
} from 'react-native';
import { Button,Icon } from 'react-native-elements';
import { NavigationBar } from 'navigationbar-react-native';
import { StatusBarBackground } from '../../status';
import httpServices from "../../services/http";
import Swiper from 'react-native-swiper';

export default class HomeScreen extends React.Component {

   ComponentLeft= () => {
        return(
            <View style={{ flex: 1 }}>
                <Text  style={styles.fontAll}>Home</Text>
            </View>
        );
    };

     ComponentRight = () => {
        return(
            <View style={{ flex: 1, alignItems: 'flex-end', }}>
                <TouchableOpacity  onPress={()=>{
                    var me =this;
                    this._getToken(function () {
                        if(me.state.token==null) {
                            me.state.navigation.push("Login")
                        }else {
                            me.state.navigation.push("Menu")
                        }
                    });

                }}>
                    <Icon name="menu" style={{ color: 'white',marginRight: 20 }}></Icon> 
                </TouchableOpacity>
            </View>
        );
    };

    state = {
        navigation:this.props.navigation,
        setting:[],
        token:null,
        campaign:[]
    };
    constructor(props){
        super(props);
        var me =this;
       // this._saveToken("2a5723d62800fe8855dff709af631004")
        httpServices.get("setting/getSetting.php",function (data) {

            me.setState({setting:data.data});
        });
        httpServices.get("campaign/campaign.php",function (data) {
            console.log(data)
            me.setState({campaign:data.data});
        });
    }
    async _saveToken(token){
        try {
            await AsyncStorage.setItem('token', token);
        } catch (error) {
            // Error saving data
        }
    }
    async _getToken (callback) {
        try {
            this.setState({token:null})
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                // We have data!!
                this.setState({token:value})
                callback();
            }else {
                callback();
            }

                  } catch (error) {
                  // Error retrieving data
              }
                  }

                  render() {

                  var width = Dimensions.get('window').width;
                  const styles = StyleSheet.create({
                  wrapper: {
                  height:220,
                  width:width
              },
                  slide1: {
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#9DD6EB',
              },
                  slide2: {
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#97CAE5',
              },
                  slide3: {
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#92BBD9',
              },
                  text: {
                  color: '#fff',
                  fontSize: 30,
                  fontWeight: 'bold',
              }
              })
                  return (
                  <View style={{marginBottom: 50}}>

                  <NavigationBar
                  componentLeft   = { this.ComponentLeft}
                  componentRight    = { this.ComponentRight  }
                  navigationBarStyle= {{ backgroundColor: '#333333' }}
                  statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#333333' }}
                  />

                  <ScrollView>

                  <View>
                  <Swiper style={styles.wrapper} >
                  {  this.state.campaign.map((item, index) => (
                      <View >
                          <Image source={{uri:'https://maxxis-warranty.com/images/campaign/'+item.image}}  style={{width:width,height:220}}/>
                      </View>
                      ))
                  }
              </Swiper>

              <TouchableOpacity onPress={()=>this.state.navigation.push("Product")}>
                 <Image source={require('../../assets/image/list2.png')} />
              </TouchableOpacity>
          </View>
          <View
            style={{
              justifyContent: 'space-between',
              flexDirection: 'row',
            }}>
              <TouchableOpacity onPress={()=>{
                  var me =this;
                  this._getToken(function () {
                      if(me.state.token==null) {
                          me.state.navigation.push("Login")
                      }else {
                          me.state.navigation.push("RegisterWheel")
                      }
                  });

              }}>
                 <Image source={require('../../assets/image/list3.png')} style={{width:187,height:106}} />
              </TouchableOpacity>

              <TouchableOpacity onPress={()=> { this.state.navigation.push("BranchView")}}>
            <Image source={require('../../assets/image/list4.png')} style={{width:187,height:106}} />
              </TouchableOpacity>
          </View>
          <View
            style={{
              justifyContent: 'space-between',
              flexDirection: 'row',
            }}>
              <TouchableOpacity onPress={()=> {
                  var me =this;
                  this._getToken(function () {
                      if(me.state.token==null) {
                          me.state.navigation.push("Login")
                      }else {
                          me.state.navigation.push("HistoryView")
                      }
                  });

                }
              }>
                  <Image source={require('../../assets/image/list8.png')} style={{width:187,height:106}} />
              </TouchableOpacity>

            <View>
              <Image source={require('../../assets/image/list6.png')} style={{width:187,height:106}}/>

            </View>
          </View>
            <View
                style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                }}>

                <TouchableOpacity onPress={()=>this.state.navigation.push("ViewDetail", {head:"เงื่อนไขการรับประกัน",data: this.state.setting[1].key_value})}>
                    <Image source={require('../../assets/image/list5.png')} style={{width:187,height:106}} />
                </TouchableOpacity>
                <View>

                    <TouchableOpacity onPress={()=>this.state.navigation.push("ViewDetail", {head:"นโยบายความเป็นส่วนตัว",data: this.state.setting[0].key_value})}>
                        <Image source={require('../../assets/image/list7.png')}  style={{width:187,height:106}}/>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
      </View>
    );
  }
}
