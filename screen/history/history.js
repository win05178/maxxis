import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity, Dimensions, FlatList, TouchableHighlight, AsyncStorage
} from 'react-native';
import {Button, Icon, Avatar, Card, Divider, Header, CheckBox} from 'react-native-elements';
import { NavigationBar } from 'navigationbar-react-native';
import httpServices from "../../services/http";
import Images from "../../config1";
import FlexImage from "react-native-flex-image";
import HTML from "react-native-render-html";
import Tags from "react-native-tags/Tags";

const styles = StyleSheet.create({
    lineStyle:{
        borderWidth: 0.5,
        borderColor:'black',
        margin:10,
    }
});

export default class HistoryViewScreen extends React.Component {

    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>History</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })

    state = {
        navigation:this.props.navigation,
        history:[],
        token:null
    };

    async _getToken () {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                // We have data!!
                this.reload(value)
            }

        } catch (error) {
            // Error retrieving data
        }
    }

    constructor(props){
        super(props);

        this._getToken()

    }
    reload(token){
        var me =this;
        httpServices.post("history/getHistory.php",{
            "token":token
        },function (data) {
            me.setState({history:data.data});
            console.log(data.data)
        });
    }
    async getDeatil(historyid){
        return [];
    }

    check(d){
        var ret = false;
        d.forEach(function (data) {
            if(data.check)
                ret = true;
        })
        return ret;
    }
    render() {
        var width = Dimensions.get('window').width-20;
        return (
            <View style={{flex:1}}>
                <ScrollView>
                    {
                        this.state.history.map((item, index) => (
                    <View  style={{marginBottom: 30,paddingLeft: 10,paddingRight: 10}}>
                        <Text>รายการที่ {index+1}</Text>
                        <Text>สาขา</Text>
                        <Text>{item.branch_name}</Text>
                        <Text>วันที่สั่งซื้อ</Text>
                        <Text>{item.buy_date}</Text>
                        <Text>เลขที่เอกสาร</Text>
                        <Text>{item.doc_no}</Text>
                        <View style={{flexDirection:'row'}}>

                            <Text style={{width:width/4,justifyContent: 'center',alignItems:'center'}}>รุ่นยาง</Text>
                            <Text style={{width:width/4,justifyContent: 'center',alignItems:'center'}}>Serial</Text>
                            <Text style={{width:width/4,justifyContent: 'center',alignItems:'center'}}>สถานะ</Text>
                            <Text style={{width:width/4,justifyContent: 'center',alignItems:'center'}}>เคลม</Text>
                        </View>
                        {
                            item.data.map((item1, index1) => (
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{width:width/4,paddingTop: 15}}>{item1.product_name}</Text>
                                    <Text style={{width:width/4,paddingTop: 15}}>{item1.sn}</Text>
                                    <Text style={{width:width/4,paddingTop: 15}}>{item1.status_name}</Text>
                                    {(item1.status==="1")?(<CheckBox
                                        center
                                        checked={item1.check}
                                        containerStyle={{backgroundColor: 'white',borderColor: 'white'}}
                                        onPress={() => {
                                            var i = this.state.history;
                                            i[index].data[index1].check = ! i[index].data[index1].check;
                                            this.setState({history:i})}}
                                    />):null}

                                </View>
                                ))
                        }
                        <View   >
                            <Button textStyle={{color:'#5c6573', fontFamily:'kanit'}} disabled={
                                (this.check(item.data)==true)?false:true
                            } title={"ส่งเคลม"} onPress={()=>this.state.navigation.push("CliamView",{cliam:item.data})}   />
                        </View>
                    </View>
                    ))
                    }

                </ScrollView>
            </View>
        );
    }
}
