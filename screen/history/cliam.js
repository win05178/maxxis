import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity, Dimensions, FlatList, TouchableHighlight, AsyncStorage, Picker
} from 'react-native';
import {Button, Icon, Avatar, Card, Divider, Header, CheckBox} from 'react-native-elements';
import { NavigationBar } from 'navigationbar-react-native';
import httpServices from "../../services/http";
import Images from "../../config1";
import FlexImage from "react-native-flex-image";
import HTML from "react-native-render-html";
import Tags from "react-native-tags/Tags";

const styles = StyleSheet.create({
    lineStyle:{
        borderWidth: 0.5,
        borderColor:'black',
        margin:10,
    }
});

export default class CliamViewScreen extends React.Component {

    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>เคลมสินค้า</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })

    state = {
        navigation:this.props.navigation,
        cliam:this.props.navigation.getParam("cliam"),
        token:null,
        province_id:null,
        branch_name:null,
        branch_data:[],
        province:[]
    };

    async _getToken () {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                // We have data!!
                this.reload(value)
            }

        } catch (error) {
            // Error retrieving data
        }
    }

    constructor(props){
        super(props);

       console.log(this.state.cliam);
       var me =this;
        httpServices.get("province/getProvince.php",function (data) {
            me.setState({province:data.data});
            me.getBranch(data.data[0].name);
        });
    }

    getBranch(itemValue){
        this.setState({province_id:itemValue})
        var me = this;
        httpServices.get("branch/getBranch.php?province="+itemValue,function (data) {
            me.setState({branch_data:data.data});
            me.setState({branch_name:data.data[0].id})
        });
    }

    reload(token){
        var me =this;
        var c = this.state.cliam;

        c.branch = this.state.branch_name;
        console.log(JSON.stringify(c))
        httpServices.post("register_wheel/cliam.php",{
            "data":JSON.stringify(c),
            "branch":this.state.branch_name,
            "token":token
        },function (data) {
            console.log(data)
            me.state.navigation.popToTop()
        });
    }
    cliam(){
        this._getToken()

    }



    render() {
        var width = Dimensions.get('window').width;
        return (
            <View style={{flex:1}}>
                <ScrollView>
                    <View>
                        <Text style={{color:'#f04c24'}}>จังหวัดที่เคลม</Text>
                        <Picker
                            selectedValue={this.state.province_id}
                            style={{ height: 50, width: width }}
                             onValueChange={(itemValue, itemIndex) =>this.getBranch(itemValue)   }>

                            {
                                this.state.province.map((item, index) => (
                                    <Picker.Item label={item.name} value={item.name}  />
                                ))
                            }

                        </Picker>
                    </View>
                    <View>
                        <Text style={{color:'#f04c24'}}>ชื่อร้านที่เคลม</Text>
                        <Picker
                            selectedValue={this.state.branch_name}
                            style={{ height: 50, width: width }}
                            onValueChange={(itemValue, itemIndex) =>{

                                this.setState({branch_name:itemValue})   }}>
                            {
                                this.state.branch_data.map((item, index) => (
                                    <Picker.Item label={item.branch_name} value={item.id}  />
                                ))
                            }
                        </Picker>
                    </View>
                    <View   >
                        <Button textStyle={{color:'#5c6573', fontFamily:'kanit'}} title={"ส่งเคลม"} onPress={()=>this.cliam()}   />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
