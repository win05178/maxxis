import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity ,
} from 'react-native';
import { Button,Icon ,Avatar} from 'react-native-elements';
import { NavigationBar } from 'navigationbar-react-native';
import httpServices from "../../services/http";
import Images from "../../config1";

export default class PromotionScreen extends React.Component {


    state = {
        navigation:this.props.navigation,
        promotion:[]
    };
    ComponentLeft= () => {
        return(
            <View style={{ flex: 1 }}>
                <Text  style={styles.fontAll}>โปรโมชั่น</Text>
            </View>
        );
    };

    constructor(props){
        super(props);
        var me =this;
        httpServices.get("promotion/getPromotion.php",function (data) {
            me.setState({promotion:data.data});
        });
    }


    render() {
        return (
            <View style={{ flex: 1,marginBottom: 50 }}>
                <NavigationBar
                    componentLeft   = { this.ComponentLeft}

                    navigationBarStyle= {{ backgroundColor: '#333333' }}
                    statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#333333' }}
                />

                <ScrollView>
                    {
                        this.state.promotion.map((item, index) => (

                                <TouchableOpacity
                                    key = {item.id} style={{marginBottom: 5}} onPress={()=>this.state.navigation.push("ViewDetail", {head: item.promotion_name,data: item.promotion_detail})}>
                                    <View  style={{flexDirection:'row', flexWrap:'wrap'}}>
                                        <Image   source={{uri: item.promotion_image}} style={{width:100,height:100}} />
                                        <View  style={{paddingLeft: 20,paddingRight: 20}}>
                                            <Text style={{ fontFamily: 'kanit'}}>{item.promotion_name}</Text>
                                            <Text style={{ fontFamily: 'kanit'}}>{item.promotion_detail}</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            )
                        )
                    }
                </ScrollView>
            </View>
        );
    }
}
