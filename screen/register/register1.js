import React from "react";
import {StyleSheet, Text, View, Image, TextInput,TouchableOpacity} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Button } from 'react-native-elements';

export default class Register1Screen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>Register1</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })
    state = { profile:{Email: '' ,Password:'',Birthday:null,Name:'',LastName:'',Job:'',Phone:''},   navigation:this.props.navigation,  isDateTimePickerVisible: false,};

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        this.state.profile.Birthday = date;
        this.setState(this.state);
        this._hideDateTimePicker();
    };


    formatDate(date){
        if(date ==null)
            return "--/--/---";
        else{
            var d = date.getDate();
            var m = date.getMonth()+1;
            var y = date.getFullYear();
            return d+"/"+m+"/"+y;
        }
    }

    render() {
        return (
            <View style={{padding:20}}>
                <View>
                    <Text>อีเมล์</Text>
                    <TextInput
                        textContentType={'emailAddress'}
                        keyboardType={'email-address'}
                        style={{  fontFamily:'kanit',}}
                        placeholder='กรุณากรอกข้อมูล'
                        onChangeText={(text) => {
                            this.state.profile.Email = text;
                            this.setState(this.state);
                        }}
                        value={this.state.profile.Email}
                    />
                </View>
                <View>
                    <Text>รหัสผ่าน</Text>
                    <TextInput
                        textContentType={'password'}
                        keyboardType={'default'}
                        style={{  fontFamily:'kanit',}}
                        secureTextEntry={true}
                        placeholder='กรุณากรอกข้อมูล'
                        onChangeText={(text) => {
                            this.state.profile.Password = text;
                            this.setState(this.state);
                        }}
                        value={this.state.profile.Password}
                    />
                </View>
                <View>
                    <Text>ชื่อ</Text>
                    <TextInput

                        style={{  fontFamily:'kanit',}}
                        placeholder='กรุณากรอกข้อมูล'

                        onChangeText={(text) => {
                            this.state.profile.Name = text;
                            this.setState(this.state);
                        }}
                        value={this.state.profile.Name}
                    />
                </View>
                <View>
                    <Text>นามสกุล</Text>
                    <TextInput

                        style={{  fontFamily:'kanit',}}
                        placeholder='กรุณากรอกข้อมูล'
                        onChangeText={(text) => {
                            this.state.profile.LastName = text;
                            this.setState(this.state);
                        }}
                        value={this.state.profile.LastName}
                    />
                </View>
                <View>
                    <Text>เบอร์โทร</Text>
                    <TextInput
                        textContentType={'telephoneNumber'}
                        keyboardType={'phone-pad'}
                        style={{  fontFamily:'kanit',}}
                        placeholder='กรุณากรอกข้อมูล'
                        onChangeText={(text) => {
                            this.state.profile.Phone = text;
                            this.setState(this.state);
                        }}
                        value={this.state.profile.Phone}
                    />
                </View>
                <View>
                    <Text>วันเกิด</Text>
                    <View style={{flexDirection:'row'}}>
                        <Text>{this.formatDate(this.state.profile.Birthday)}</Text>
                        <Button textStyle={styles.buttonLogin} title={"เลือกวันที่"}  onPress={this._showDateTimePicker} buttonStyle={{marginLeft:10}} />
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                        />
                    </View>
                </View>
                <View>
                    <Text>อาชีพ</Text>
                    <TextInput

                        style={{  fontFamily:'kanit',}}
                        placeholder='กรุณากรอกข้อมูล'
                        onChangeText={(text) => {
                            this.state.profile.Job = text;
                            this.setState(this.state);
                        }}
                        value={this.state.profile.Job}
                    />
                </View>
                <View   >
                    <Button textStyle={styles.buttonLogin} title={"ถัดไป"} backgroundColor='red' buttonStyle={{marginTop:10}}  onPress={()=>this.state.navigation.push("Register2", {profile1: this.state.profile})} />
                </View>
            </View>
        )
    }
}