import React from "react";
import {StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Picker, Dimensions} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Button } from 'react-native-elements';
import httpServices from "../../services/http";
import * as StackActions from "react-native";
import { NavigationActions } from 'react-navigation';
export default class Register2Screen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: <Text style={ {fontFamily: 'kanit'} }>Register1</Text>,
        headerStyle: {
            backgroundColor: '#333333'
        },
    })
    state = { profile1:null,  profile2:{Address:'',Province:'',Amphor:'',Tambon:'',Zipcode:''},tambon:[],province:[],amphor:[], navigation:this.props.navigation,  isDateTimePickerVisible: false,};

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        this.state.profile.Birthday = date;
        this.setState(this.state);
        this._hideDateTimePicker();
    };

    constructor(props){
        super(props);
        this.setState({profile1:this.state.navigation.getParam('profile1', null)});

        console.log(this.state.navigation.getParam('profile1', null))
        var me =this;
        httpServices.get("province/getProvince.php",function (data) {
            me.setState({province:data.data});
            var profile2 = this.state.profile2;
            profile2.Province = data.data[0].id;
            me.setState({profile2:profile2});
            me.amphor(data.data[0].id);
        });

    }

    formatDate(date){
        if(date ==null)
            return "--/--/---";
        else{
            var d = date.getDate();
            var m = date.getMonth()+1;
            var y = date.getFullYear();
            return d+"/"+m+"/"+y;
        }
    }
    amphor(id){
        var profile2 = this.state.profile2;
        profile2.Province = id;
        this.setState({profile2:profile2});
        var me =this;
        httpServices.get("amphor/getAmphor.php?province="+id,function (data) {
            me.setState({amphor:data.data});
            me.tambon(data.data[0].id)
        });

    }
    tambon(id){
        var profile2 = this.state.profile2;
        profile2.Amphor = id;
        this.setState({profile2:profile2});
        var me =this;
        httpServices.get("tambon/getTambon.php?amphor="+id,function (data) {
            me.setState({tambon:data.data});
        });

    }
    save(){
        var profile = this.state.navigation.getParam('profile1', null);
        var data = {};
        data.username = profile.Email;
        data.birthday =profile.Birthday;
        data.password=profile.Password;
        data.career=profile.Job;
        data.email=profile.Email;
        data.lastname=profile.LastName;
        data.mobile_no=profile.Phone;
        data.firstname=profile.Name;

        data.address =this.state.profile2.Address;
        data.province=this.state.profile2.Province;
        data.amphur=this.state.profile2.Amphor;
        data.tambon=this.state.profile2.Tambon;
        data.zipcode=this.state.profile2.Zipcode;


        console.log(JSON.stringify(data))
        var me = this;
        httpServices.post("user/addUser.php",{data:JSON.stringify(data)},function (data) {

            me.state.navigation.popToTop()
        });
    }

    render() {
        var width = Dimensions.get('window').width;
        return (
            <View style={{padding:20}}>
                <View>
                    <Text>ที่อยู่</Text>
                    <TextInput

                        style={{  fontFamily:'kanit',}}
                        placeholder='กรุณากรอกข้อมูล'
                        onChangeText={(text) => {
                            this.state.profile2.Address = text;
                            this.setState(this.state);
                        }}
                        value={this.state.profile2.Address}
                    />
                </View>
                <View>
                    <Text>จังหวัด</Text>
                    <Picker
                        selectedValue={this.state.profile2.Province}
                        style={{ height: 50, width: width }}
                        onValueChange={(itemValue, itemIndex) => this.amphor(itemValue) }>
                        {
                            this.state.province.map((item, index) => (
                                <Picker.Item label={item.name} value={item.id}  />
                            ))
                        }
                    </Picker>
                </View>
                <View>
                    <Text>อำเภอ</Text>
                    <Picker
                        selectedValue={this.state.profile2.Amphor}
                        style={{ height: 50, width: width }}
                        onValueChange={(itemValue, itemIndex) => this.tambon(itemValue) }>
                        {
                            this.state.amphor.map((item, index) => (
                                <Picker.Item label={item.name} value={item.id}  />
                            ))
                        }
                    </Picker>
                </View>
                <View>
                    <Text>ตำบล</Text>
                    <Picker
                        selectedValue={this.state.profile2.Tambon}
                        style={{ height: 50, width: width }}
                        onValueChange={(itemValue, itemIndex) =>    { var profile2 = this.state.profile2;
                            profile2.Tambon = itemValue;
                            this.setState({profile2:profile2})
                             }}>
                        {
                            this.state.tambon.map((item, index) => (
                                <Picker.Item label={item.name} value={item.id}  />
                            ))
                        }
                    </Picker>
                </View>
                <View>
                    <Text>รหัสไปรษณีย์</Text>
                    <TextInput
                        textContentType={'postalCode'}
                        keyboardType={'number-pad'}
                        style={{  fontFamily:'kanit',}}
                        placeholder='กรุณากรอกข้อมูล'
                        onChangeText={(text) => {
                            this.state.profile2.Zipcode = text;
                            this.setState(this.state);
                        }}
                        value={this.state.profile2.Zipcode}
                    />
                </View>
                <View   >
                    <Button textStyle={styles.buttonLogin} title={"บันทึก"} backgroundColor='red' onPress={()=> this.save()} buttonStyle={{marginTop:10}} />
                </View>
            </View>
        )
    }
}