import React from "react";
import { View, Text } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import MainScreen from "./screen/main/main";
import ViewDetailScreen from "./screen/View/viewDetail";
import ProductScreen from "./screen/product/product";
import ProductDetailScreen from "./screen/product/productDetail";
import ProductViewScreen from "./screen/product/productView";
import MenuScreen from "./screen/menu/menu";
import LoginScreen from "./screen/login/login";
import Register1Screen from "./screen/register/register1"
import Register2Screen from "./screen/register/register2"
import RegisterWheelScreen from "./screen/registerWheel/registerWheel"
import getProductScreen from "./screen/registerWheel/getProduct"
import takePhoto from "./screen/registerWheel/takePhoto"
import HistoryViewScreen from "./screen/history/history"
import CliamViewScreen from "./screen/history/cliam"
import BranchViewScreen from "./screen/branch/branch"

const AppNavigator = createStackNavigator({
    Main: {
        screen: MainScreen
    },
    ViewDetail: {
        screen: ViewDetailScreen
    },
    Product: {
        screen: ProductScreen
    },
    ProductDetail: {
        screen: ProductDetailScreen
    },
    ProductView: {
        screen: ProductViewScreen
    },
    Menu: {
        screen: MenuScreen
    },
    Login:{
        screen: LoginScreen
    },
    Register1:{
        screen: Register1Screen
    },
    Register2:{
        screen: Register2Screen
    },
    RegisterWheel:{
        screen: RegisterWheelScreen
    },
    AddProduct:{
        screen:getProductScreen
    },
    takePhoto:{
        screen:takePhoto
    },
    HistoryView:{
        screen:HistoryViewScreen
    },
    CliamView:{
        screen:CliamViewScreen
    },
    BranchView:{
        screen:BranchViewScreen
    }
});
styles = require('./style/style');
export default createAppContainer(AppNavigator);