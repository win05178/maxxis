'use strict';
var React = require('react-native');

var {
    StyleSheet,
} = React;

module.exports = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2a2a2a',
        alignItems: 'center',
        justifyContent: 'center',

    },
    fontWhite:{
        fontFamily:'kanit',
        color:'#fff'
    },
    fontInputLogin: {
        height: 40, flex: 1, color: '#5c6573', borderBottomWidth: 1, borderColor: "#fff", fontFamily: 'kanit',
        marginLeft: 20
    },
    buttonLogin:{
        fontFamily:'kanit',
        color:'#fff'
    },
    fontAll:{
        fontFamily: 'kanit'
    },
});